up:
	docker-compose --project-name theia --file ./docker-compose.yml up -d 

down:
	docker-compose --project-name theia --file ./docker-compose.yml down
